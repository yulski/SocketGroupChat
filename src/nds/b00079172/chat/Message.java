package nds.b00079172.chat;

import java.io.*;

/**
 * The means of communication between clients and the server and vice versa. All
 * communication is carried out through Message objects.
 * 
 * @author julek
 *
 */
public class Message implements Serializable {

	/**
	 * A standard chat message sent between clients.
	 */
	public static final int STANDARD_MESSAGE = 0;

	/**
	 * A message sent from the client to the server, requesting to use a
	 * particular username. The proposed username should be included as the
	 * textBody of the message.
	 */
	public static final int USERNAME_PROPOSAL_MESSAGE = 1;

	/**
	 * A message sent from the server to the client, indicating that a requested
	 * username is available and can be used by the client.
	 */
	public static final int USERNAME_ACCEPTED_MESSAGE = 2;

	/**
	 * A message sent from the server to the client, indicating that a requested
	 * username is not available and cannot be used by the client.
	 */
	public static final int INCORRECT_USERNAME_MESSAGE = 3;

	/**
	 * A status message sent from one client to other clients. For example, a
	 * client informing other clients that it has connected to or disconnected
	 * from the chat.
	 */
	public static final int STATUS_MESSAGE = 4;

	private String author;
	private String textBody;
	private File file;
	private int messageType;
	private int id;

	/**
	 * Create a new message of a particular type, with an author and a text
	 * body.
	 * 
	 * @param type
	 *            The type of the message. This must be one of the constants
	 *            defined in this class.
	 * @param author
	 *            The username of the user who sent this message.
	 * @param textBody
	 *            The text to put in the message body.
	 */
	public Message(int type, String author, String textBody) {
		this.setMessageType(type);
		this.author = author;
		this.textBody = textBody;
		this.file = null;
	}

	/**
	 * Create a new message of a particular type, with an author and a text
	 * body, as well as a file.
	 * 
	 * @param type
	 *            The type of the message. This must be one of the constants
	 *            defined in this class.
	 * @param author
	 *            The username of the user who sent this message.
	 * @param textBody
	 *            The text to put in the message body.
	 * @param file
	 *            The file to include in the message
	 */
	public Message(int type, String author, String textBody, File file) {
		this.setMessageType(type);
		this.author = author;
		this.textBody = textBody;
		this.file = file;
	}

	/**
	 * Get the username of the message author.
	 * 
	 * @return Author's username.
	 */
	public String getAuthor() {
		return this.author;
	}

	/**
	 * Get a String which contains the formatted message in the way the user
	 * should see it.
	 * 
	 * @return Formatted message the way the user should see it
	 */
	public String getDisplayString() {
		String str = this.author + ": " + this.textBody;
		// if the message contains a file, include it's name in the string
		if (this.file != null) {
			str += "\n" + this.file.getName();
		}
		str += "\n";
		return str;
	}

	/**
	 * Get the file in the message.
	 * 
	 * @return The file in the message
	 */
	public File getFile() {
		return this.file;
	}

	/**
	 * Get the message id.
	 * 
	 * @return The message id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Get the message type.
	 * 
	 * @return The message type
	 */
	public int getMessageType() {
		return this.messageType;
	}

	/**
	 * Get the text body of the message.
	 * 
	 * @return The message text body
	 */
	public String getTextBody() {
		return this.textBody;
	}

	/**
	 * Determines if the message includes a file.
	 * 
	 * @return true if the message includes a file, false otherwise
	 */
	public boolean hasFile() {
		return this.file != null;
	}

	/**
	 * Set the author of the message.
	 * 
	 * @param author
	 *            The username of the message author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * Set the file to include in the message.
	 * 
	 * @param file
	 *            The file to be included in the message
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * Set the message id.
	 * 
	 * @param id
	 *            The message id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Set the type of message this is.
	 * 
	 * @param type
	 *            The message type. If this is not one of the constants defined
	 *            in this class, it will be changed to STANDARD_MESSAGE
	 */
	public void setMessageType(int type) {
		// check the type against all message type constants and if there's no
		// match, change it to STANDARD_MESSAGE
		if (type != STANDARD_MESSAGE && type != USERNAME_ACCEPTED_MESSAGE
				&& type != INCORRECT_USERNAME_MESSAGE && type != STATUS_MESSAGE) {
			type = STANDARD_MESSAGE;
		}
		this.messageType = type;
	}

	/**
	 * Set the text body of the message.
	 * 
	 * @param textBody
	 *            The message text body
	 */
	public void setTextBody(String textBody) {
		this.textBody = textBody;
	}

}
