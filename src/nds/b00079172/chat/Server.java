package nds.b00079172.chat;

import java.io.*;
import java.net.*;

/**
 * Chat server listening for client connections.
 * 
 * @author julek
 *
 */
public class Server implements Runnable {

	private static final int DEFAULT_PORT = 8888;

	private ChatSession currentSession;
	private ServerSocket serverSocket;

	/**
	 * Create a new chat Server listening for connections on a particular port.
	 * 
	 * @param port
	 *            The port to open a ServerSocket on.
	 * @throws IOException
	 *             If the ServerSocket failed to open.
	 */
	public Server(int port) throws IOException {
		this.serverSocket = new ServerSocket(port);
	}

	/**
	 * Listen for client connections, create chat sessions, and dispatch workers
	 * to handle clients.
	 */
	public void run() {
		while (true) {
			Socket clientSocket = null;
			try {
				clientSocket = this.serverSocket.accept();
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (clientSocket != null && this.currentSession == null) {
				this.currentSession = new ChatSession();
				this.currentSession.dispatchWorker(clientSocket);
			} else if (clientSocket != null && this.currentSession != null) {
				this.currentSession.dispatchWorker(clientSocket);
			}
		}
	}

	public static void main(String[] args) {
		int port = DEFAULT_PORT;

		// if arguments were passed, try to use first arg as the port number
		if (args.length > 0) {
			try {
				port = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
			}
		}

		Server server = null;

		try {
			server = new Server(port);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// if server created successfully, start it and print "running" message
		if (server != null) {
			new Thread(server).start();
			System.out.println("Server running on port " + port);
		}

	}

}
