package nds.b00079172.chat;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * A session which consists of a number of ServerWorkers and all the messages
 * sent between clients.
 * 
 * @author julek
 *
 */
public class ChatSession {

	/**
	 * List of workers handling clients.
	 */
	private List<ServerWorker> workers;

	/**
	 * List of messages sent between clients.
	 */
	private List<Message> messages;

	/**
	 * Create new ChatSession and initialize worker and messages lists.
	 */
	public ChatSession() {
		this.workers = new ArrayList<>();
		this.messages = new ArrayList<>();
	}

	/**
	 * Record a message in the messages list.
	 * 
	 * @param message
	 *            The message to be recorded
	 */
	private void recordMessage(Message message) {
		message.setId(this.messages.size());
		this.messages.add(message);
	}

	/**
	 * Dispatch a ServerWorker to handle a client connected to a particular
	 * socket.
	 * 
	 * @param clientSocket
	 *            The socket where the client is connected.
	 */
	protected void dispatchWorker(Socket clientSocket) {
		ServerWorker worker = null;
		try {
			worker = new ServerWorker(this.workers.size(), clientSocket, this);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// if worker created successfully, add it to workers list and start the
		// thread
		if (worker != null) {
			this.workers.add(worker);
			worker.start();
		}
	}

	/**
	 * End a ChatSession by stopping all workers and deleting them and deleting
	 * all messages from the session.
	 */
	public void endSession() {
		// stop and delete all workers, and delete messages
		// and files
		for (ServerWorker worker : this.workers) {
			if (worker != null) {
				worker.stopWorker();
			}
		}
		this.workers = new ArrayList<>();
		this.messages = new ArrayList<>();
	}

	/**
	 * Get a message with a particular id.
	 * 
	 * @param messageId
	 *            The id of the message.
	 * @return A message with the requested id.
	 */
	public Message getMessage(int messageId) {
		return this.messages.get(messageId);
	}

	/**
	 * Get all messages in the session.
	 * 
	 * @return A List of all messages sent between clients in the current
	 *         session.
	 */
	public List<Message> getMessages() {
		return this.messages;
	}

	/**
	 * Determines if the session has ended.
	 * 
	 * @return true if session is over, false otherwise.
	 */
	public boolean isSessionOver() {
		for (ServerWorker worker : this.workers) {
			if (worker != null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Determines if a particular username is available.
	 * 
	 * @param username
	 *            The username to check availability of.
	 * @return true if username is available, false otherwise
	 */
	public boolean isUsernameAvailable(String username) {
		for (ServerWorker worker : this.workers) {
			if (worker != null) {
				String workerUsername = worker.getClientUsername();
				if (workerUsername.equals(username)) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Relay a message through workers to all clients participating in the chat
	 * session.
	 * 
	 * @param message
	 *            The message to be passed to all clients.
	 */
	public void relayMessage(Message message) {
		for (ServerWorker worker : this.workers) {
			if (worker != null) {
				worker.sendMessageToClient(message);
			}
		}
		this.recordMessage(message);
	}

	/**
	 * Relay a message through workers to all clients participating in the chat
	 * session except for the worker with a particular index.
	 * 
	 * @param index
	 *            The index of the worker who should not be passed the message.
	 * @param message
	 *            The message to be passed.
	 */
	public void relayMessageToOthers(int index, Message message) {
		for (int i = 0; i < this.workers.size(); i++) {
			if (i != index && this.workers.get(i) != null) {
				this.workers.get(i).sendMessageToClient(message);
			}
		}
		this.recordMessage(message);
	}

	/**
	 * Remove a worker from the session.
	 * 
	 * @param index
	 *            Index of the worker to be removed.
	 */
	public void removeWorker(int index) {
		// set the worker to null because index has to be preserved
		this.workers.set(index, null);
		if (this.isSessionOver()) {
			this.endSession();
		}
	}

}
