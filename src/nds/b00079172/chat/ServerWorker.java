package nds.b00079172.chat;

import java.net.*;
import java.util.List;
import java.io.*;

/**
 * A Thread handling communication between the chat server and a client
 * connected to the server.
 * 
 * @author julek
 *
 */
public class ServerWorker extends Thread {

	private int index;
	private Socket socket;
	private ChatSession session;
	private String clientUsername;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private boolean stopped;
	private boolean initialized;

	/**
	 * Create a new ServerWorker to handle some client connected to the chat
	 * server.
	 * 
	 * @param index
	 *            The index of this worker in the workers list of the current
	 *            chat session.
	 * @param socket
	 *            The socket connected to the client.
	 * @param session
	 *            The chat session hosting this particular connection between
	 *            ServerWorker and Client.
	 * @throws IOException
	 *             If failed to open an input/output stream on the socket.
	 */
	public ServerWorker(int index, Socket socket, ChatSession session) throws IOException {
		this.index = index;
		this.socket = socket;
		this.clientUsername = "";
		this.session = session;
		this.stopped = false;
		this.initialized = false;
		this.output = new ObjectOutputStream(this.socket.getOutputStream());
		this.input = new ObjectInputStream(this.socket.getInputStream());
	}

	/**
	 * Initialize the connection between this worker and the client. First,
	 * expect a username request from the client. Then check if the username is
	 * available and send the response. Finally, if the username is available,
	 * relay a status message through the parent ChatSession informing all other
	 * clients that the client connected to this ServerWorker has connected to
	 * the chat server.
	 */
	private void initialize() {
		try {
			Message inMsg = (Message) this.input.readObject();
			if (!this.session.isUsernameAvailable(inMsg.getTextBody())) {
				Message errorMessage = new Message(Message.INCORRECT_USERNAME_MESSAGE, null, null);
				this.sendMessageToClient(errorMessage);
				this.stopWorker();
				this.session.removeWorker(this.index);
				return;
			}
			this.clientUsername = inMsg.getTextBody();
			Message acceptedMessage = new Message(Message.USERNAME_ACCEPTED_MESSAGE, null, null);
			this.output.writeObject(acceptedMessage);
			this.initialized = true;
			Message helloMessage = new Message(Message.STATUS_MESSAGE, this.clientUsername,
					this.clientUsername + " connected");
			this.session.relayMessageToOthers(this.index, helloMessage);
			this.sendSessionToClient();
		} catch (IOException | ClassNotFoundException e) {
			// stop worker if initialization failed
			this.stopWorker();
			this.session.removeWorker(this.index);
		}
	}

	/**
	 * Get the ChatSession hosting this ServerWorker.
	 * 
	 * @return The ChatSession hosting this ServerWorker
	 */
	public ChatSession getSession() {
		return session;
	}

	/**
	 * Get the socket connected to the client.
	 * 
	 * @return The socket connected to the client.
	 */
	public Socket getSocket() {
		return socket;
	}

	/**
	 * Get the username used by the client connected to this ServerWorker.
	 * 
	 * @return The username used by connected client
	 */
	public String getClientUsername() {
		return clientUsername;
	}

	/**
	 * Determines if the ServerWorker is initialized.
	 * 
	 * @return true if initialized successfully, false otherwise
	 */
	public boolean isInitialized() {
		return this.initialized;
	}

	/**
	 * Determines if the ServerWorker is stopped or not.
	 * 
	 * @return true if stopped, false otherwise
	 */
	public boolean isStopped() {
		return stopped;
	}

	/**
	 * Listen for incoming messages from the client and send them to the parent
	 * ChatSession to be relayed to all Clients participating in the chat.
	 */
	@Override
	public void run() {
		while (!this.stopped) {
			if (!this.initialized) {
				this.initialize();
				continue;
			}

			Message inMsg = null;
			try {
				inMsg = (Message) this.input.readObject();
			} catch (IOException e) {
				// this exception may be an indication of
				// socket being closed on the other end, so
				// just exit
				this.stopWorker();
				this.session.removeWorker(this.index);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			if (inMsg != null) {
				this.session.relayMessage(inMsg);
			}
		}
	}

	/**
	 * Send a message to the connected client.
	 * 
	 * @param message
	 *            The message to be sent to the client
	 */
	public void sendMessageToClient(Message message) {
		try {
			this.output.writeObject(message);
		} catch (IOException e) {
			// stop worker if can't send messages
			// to client
			this.stopWorker();
			this.session.removeWorker(this.index);
		}
	}

	/**
	 * Send all messages in the parent chat session to the connected client.
	 * When a client first connects to a chat, all messages sent previously in
	 * that chat session can be sent to them.
	 */
	public void sendSessionToClient() {
		List<Message> messages = this.session.getMessages();
		for (Message message : messages) {
			this.sendMessageToClient(message);
		}
	}

	/**
	 * Set the parent ChatSession.
	 * 
	 * @param session
	 *            The parent ChatSession
	 */
	public void setSession(ChatSession session) {
		this.session = session;
	}

	/**
	 * Set the socket.
	 * 
	 * @param socket
	 *            The socket
	 */
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	/**
	 * Stop the worker. This involves sending a status "disconnected" message to
	 * other clients and ceasing listening for any incoming messages. Finally,
	 * the input/output streams and the socket are closed.
	 */
	public void stopWorker() {
		System.out.println("Stopping worker " + this.index);
		if (this.initialized) {
			Message disconnectMessage = new Message(Message.STATUS_MESSAGE, this.clientUsername,
					this.clientUsername + " disconnected");
			this.session.relayMessageToOthers(this.index, disconnectMessage);
		}
		this.stopped = true;
		try {
			this.input.close();
			this.output.close();
			this.socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
