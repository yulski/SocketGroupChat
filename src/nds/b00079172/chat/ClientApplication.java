package nds.b00079172.chat;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;

import javax.activation.MimetypesFileTypeMap;

import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;

import javafx.application.*;
import javafx.collections.ObservableList;
import javafx.geometry.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.event.*;

/**
 * The main client application. Includes the GUI and all event handling for the
 * GUI.
 * 
 * @author julek
 *
 */
public class ClientApplication extends Application {

	private static final String TITLE = "Socket Group Chat";

	private static final int WELCOME_WIDTH = 240;
	private static final int WELCOME_HEIGHT = 120;

	private static final int CHAT_WIDTH = 500;
	private static final int CHAT_HEIGHT = 440;

	private static final int CHAT_AREA_WIDTH = 490;
	private static final int CHAT_AREA_HEIGHT = 400;
	private static final int INPUT_AREA_WIDTH = 490;
	private static final int INPUT_AREA_HEIGHT = 60;

	private static final int EMOJI_PANE_WIDTH = 180;
	private static final int EMOJI_PANE_HEIGHT = 400;

	private static final int IMAGE_WIDTH = 150;
	private static final int IMAGE_HEIGHT = 100;

	private static final Color LOCAL_CLIENT_MESSAGE_BACKGROUND = Color.CORNFLOWERBLUE;
	private static final Color REMOTE_CLIENT_MESSAGE_BACKGROUND = Color.SALMON;
	private static final Color STATUS_MESSAGE_BACKGROUND = Color.THISTLE;

	private static final int MAX_FILE_SIZE = 20000000;

	private Stage primaryStage;

	private TextField usernameField;
	private TextField addressField;
	private TextField portField;
	private Button connectButton;
	private Scene welcomeScene;

	private VBox chatBox;
	private TextArea inputArea;
	private Button sendButton;
	private Button selectFileButton;
	private Button cancelFileButton;
	private Label currentFileLabel;
	private Scene chatScene;

	private FileChooser fileChooser;

	private File uploadFile;
	private File downloadFile;

	private Client client;

	/**
	 * Start the application. In this method, the entire GUI is created and all
	 * event listeners are bound to their targets.
	 */
	public void start(Stage primaryStage) throws Exception {

		this.primaryStage = primaryStage;

		// welcome scene
		Label usernameLabel = new Label("username");
		this.usernameField = new TextField();

		Label addressLabel = new Label("IP address");
		this.addressField = new TextField();

		Label portLabel = new Label("port no.");
		this.portField = new TextField();

		this.connectButton = new Button("Connect");

		GridPane welcomePane = new GridPane();
		welcomePane.setMinSize(WELCOME_WIDTH, WELCOME_HEIGHT);
		welcomePane.setMaxSize(WELCOME_WIDTH, WELCOME_HEIGHT);
		welcomePane.setPadding(new Insets(5, 5, 5, 5));
		welcomePane.setVgap(5);
		welcomePane.setHgap(5);

		welcomePane.add(usernameLabel, 0, 0);
		welcomePane.add(this.usernameField, 1, 0);
		welcomePane.add(addressLabel, 0, 1);
		welcomePane.add(this.addressField, 1, 1);
		welcomePane.add(portLabel, 0, 2);
		welcomePane.add(this.portField, 1, 2);
		welcomePane.add(this.connectButton, 0, 3);

		this.welcomeScene = new Scene(welcomePane);

		// chat scene
		this.chatBox = new VBox(10);
		this.chatBox.setPrefSize(CHAT_AREA_WIDTH, CHAT_AREA_HEIGHT);
		this.chatBox.setBackground(new Background(
				new BackgroundFill(Color.WHITE, new CornerRadii(0), new Insets(0, 0, 0, 0))));
		this.chatBox.setPadding(new Insets(5, 5, 5, 5));

		ScrollPane chatScrollPane = new ScrollPane(this.chatBox);
		chatScrollPane.setPrefSize(CHAT_AREA_WIDTH, CHAT_AREA_HEIGHT);
		chatScrollPane.vvalueProperty().bind(this.chatBox.heightProperty());

		this.inputArea = new TextArea();
		this.inputArea.setPrefSize(INPUT_AREA_WIDTH, INPUT_AREA_HEIGHT);

		this.sendButton = new Button("Send");

		this.selectFileButton = new Button("Select File");

		this.cancelFileButton = new Button("Cancel File");

		this.currentFileLabel = new Label("");

		GridPane chatPane = new GridPane();
		chatPane.setMinSize(CHAT_WIDTH, CHAT_HEIGHT);
		chatPane.setPadding(new Insets(5, 5, 5, 5));
		chatPane.setVgap(5);
		chatPane.setHgap(5);

		FlowPane chatButtonPane = new FlowPane();

		ObservableList<Node> chatButtonPaneChildren = chatButtonPane.getChildren();
		chatButtonPaneChildren.add(this.sendButton);
		chatButtonPaneChildren.add(this.selectFileButton);
		chatButtonPaneChildren.add(this.cancelFileButton);
		chatButtonPaneChildren.add(this.currentFileLabel);

		chatPane.add(chatScrollPane, 0, 0);
		chatPane.add(this.inputArea, 0, 1);
		chatPane.add(chatButtonPane, 0, 2);

		// emoji pane
		FlowPane emojiPane = new FlowPane();
		emojiPane.setPrefSize(EMOJI_PANE_WIDTH, EMOJI_PANE_HEIGHT);

		ScrollPane emojiScrollPane = new ScrollPane(emojiPane);
		emojiScrollPane.setPrefSize(EMOJI_PANE_WIDTH, EMOJI_PANE_HEIGHT);

		Collection<Emoji> allEmojis = EmojiManager.getAll();

		// emoji event handlers
		EventHandler<MouseEvent> emojiHoverHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				EventType<?> eventType = e.getEventType();
				Label emojiLabel = (Label) e.getSource();
				Color fillColor = Color.BLACK;
				if (eventType.equals(MouseEvent.MOUSE_ENTERED)) {
					fillColor = Color.BLUE;
				}
				emojiLabel.setTextFill(fillColor);
			}
		};

		EventHandler<MouseEvent> emojiClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				Label emojiLabel = (Label) e.getSource();
				String emoji = emojiLabel.getText();
				ClientApplication.this.inputArea.appendText(emoji);
			}
		};

		ObservableList<Node> emojiPaneChildren = emojiPane.getChildren();
		Font emojiPaneFont = new Font(16);

		for (Emoji emoji : allEmojis) {
			Label emojiLabel = new Label(emoji.getUnicode());
			emojiLabel.setFont(emojiPaneFont);
			emojiLabel.addEventFilter(MouseEvent.MOUSE_ENTERED, emojiHoverHandler);
			emojiLabel.addEventFilter(MouseEvent.MOUSE_EXITED, emojiHoverHandler);
			emojiLabel.addEventFilter(MouseEvent.MOUSE_CLICKED, emojiClickHandler);
			emojiPaneChildren.add(emojiLabel);
		}

		GridPane chatContainer = new GridPane();
		chatContainer.setMinSize(CHAT_WIDTH, CHAT_HEIGHT);
		chatContainer.setPadding(new Insets(2, 2, 2, 2));
		chatContainer.add(chatPane, 0, 0);
		chatContainer.add(emojiScrollPane, 1, 0);

		this.chatScene = new Scene(chatContainer);

		// event handlers
		EventHandler<MouseEvent> connectClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				// get username, port, and address
				String username = ClientApplication.this.usernameField.getText();
				int port = Integer.parseInt(ClientApplication.this.portField.getText());
				InetAddress address = null;
				try {
					address = InetAddress.getByName(ClientApplication.this.addressField.getText());
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				}

				// if info is ok, create new client
				if (username.length() > 0 && address != null) {
					try {
						ClientApplication.this.client = new Client(address, port, username,
								ClientApplication.this);
						ClientApplication.this.client.initialize();

						if (ClientApplication.this.client.isInitialized()) {
							// start client thread
							new Thread(ClientApplication.this.client).start();
						} else {
							ClientApplication.this.client.disconnect();
							ClientApplication.this.client = null;
							ClientApplication.this.showUsernameError();
						}
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

				// if client created successfully, show the chat scene
				if (ClientApplication.this.client != null) {
					ClientApplication.this.primaryStage.setScene(chatScene);
				}
			}
		};

		this.connectButton.addEventFilter(MouseEvent.MOUSE_CLICKED, connectClickHandler);

		EventHandler<MouseEvent> sendClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				String outMsg = ClientApplication.this.inputArea.getText().trim();

				if (outMsg.length() == 0 && !ClientApplication.this.hasUploadFile()) {
					return;
				}

				if (ClientApplication.this.hasUploadFile()) {
					File file = ClientApplication.this.uploadFile;
					ClientApplication.this.client.sendMessage(outMsg, file);
					ClientApplication.this.cancelFile();
				} else {
					ClientApplication.this.client.sendMessage(outMsg);
				}
				ClientApplication.this.clearInputTextArea();
			}
		};

		this.sendButton.addEventFilter(MouseEvent.MOUSE_CLICKED, sendClickHandler);

		EventHandler<MouseEvent> selectFileClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				ClientApplication.this.fileChooser.setTitle("Select the file to send");
				ClientApplication.this.fileChooser.setInitialFileName("");
				File selectedFile = ClientApplication.this.fileChooser
						.showOpenDialog(ClientApplication.this.primaryStage);
				if (selectedFile.length() > MAX_FILE_SIZE) {
					int maxFileSizeMB = MAX_FILE_SIZE / 1000000;
					ClientApplication.this.showErrorDialog("File size error",
							"The file is too large. The maximum allowed file size is "
									+ maxFileSizeMB + " MB.");
				} else {
					ClientApplication.this.uploadFile = selectedFile;
					ClientApplication.this.currentFileLabel.setText(selectedFile.getName());
				}
			}
		};

		this.selectFileButton.addEventFilter(MouseEvent.MOUSE_CLICKED, selectFileClickHandler);

		EventHandler<MouseEvent> cancelFileClickHandler = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				ClientApplication.this.cancelFile();
			}
		};

		this.cancelFileButton.addEventFilter(MouseEvent.MOUSE_CLICKED, cancelFileClickHandler);

		EventHandler<WindowEvent> stageCloseHandler = new EventHandler<WindowEvent>() {
			public void handle(WindowEvent e) {
				// stop client and disconnect before closing window
				if (ClientApplication.this.client != null) {
					ClientApplication.this.client.stop();
					ClientApplication.this.client.disconnect();
				}
				ClientApplication.this.primaryStage.close();
			}
		};

		this.primaryStage.addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, stageCloseHandler);

		// client
		this.client = null;

		// file chooser
		this.fileChooser = new FileChooser();

		// files
		this.uploadFile = null;
		this.downloadFile = null;

		// setting up primary stage
		this.primaryStage.setTitle(TITLE);
		this.primaryStage.setScene(this.welcomeScene);
		this.primaryStage.setResizable(false);
		this.primaryStage.show();
	}

	/**
	 * Cancel the file previously selected so it is not sent in the next
	 * message.
	 */
	private void cancelFile() {
		this.uploadFile = null;
		this.currentFileLabel.setText("");
	}

	/**
	 * Download the file currently set as downloadFile. This opens a save
	 * dialog, reads the file to be downloaded, and writes it in the selected
	 * save location.
	 */
	private void downloadFile() {
		ClientApplication.this.fileChooser.setTitle("Select a location to download the file to");
		ClientApplication.this.fileChooser.setInitialFileName(this.downloadFile.getName());
		File location = ClientApplication.this.fileChooser
				.showSaveDialog(ClientApplication.this.primaryStage);
		if (location != null) {
			try {
				ArrayList<Byte> byteList = new ArrayList<>();
				FileInputStream inStream = new FileInputStream(this.downloadFile);
				int read = inStream.read();
				while (read != -1) {
					byteList.add((byte) read);
					read = inStream.read();
				}
				inStream.close();
				FileOutputStream outStream = new FileOutputStream(location);
				Byte[] byteArr = byteList.toArray(new Byte[byteList.size()]);
				byte[] outBytes = new byte[byteArr.length];
				for (int i = 0; i < byteArr.length; i++) {
					outBytes[i] = byteArr[i];
				}
				outStream.write(outBytes);
				outStream.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Displays a simple error dialog box.
	 * 
	 * @param headerText
	 *            The text to include in the dialog header.
	 * @param contentText
	 *            The text to put in the dialog body.
	 */
	private void showErrorDialog(String headerText, String contentText) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		alert.showAndWait();
	}

	/**
	 * Add a new message to the chat area. This involves creating a Node to
	 * contain the message, styling the Node, determining if the message
	 * includes a file, attaching event handlers, displaying images.
	 * 
	 * @param message
	 *            The message that is to be displayed in the chat area.
	 */
	public void addMessageToChatArea(Message message) {
		if (message.getMessageType() == Message.STATUS_MESSAGE) {
			Label messageLabel = new Label(message.getTextBody());
			messageLabel.setBackground(new Background(new BackgroundFill(STATUS_MESSAGE_BACKGROUND,
					new CornerRadii(30), new Insets(0, 0, 0, 0))));
			messageLabel.setPadding(new Insets(10, 10, 10, 10));
			this.chatBox.getChildren().add(messageLabel);
		} else {
			Label messageLabel = new Label(message.getDisplayString());
			Color bgColor = LOCAL_CLIENT_MESSAGE_BACKGROUND;
			if (!message.getAuthor().equals(this.client.getUsername())) {
				bgColor = REMOTE_CLIENT_MESSAGE_BACKGROUND;
			}
			messageLabel.setBackground(new Background(
					new BackgroundFill(bgColor, new CornerRadii(30), new Insets(0, 0, 0, 0))));
			messageLabel.setPadding(new Insets(10, 10, 10, 10));
			if (message.hasFile()) {
				messageLabel.setUnderline(true);
				messageLabel.setCursor(Cursor.HAND);
				final File file = message.getFile();
				String fileName = file.getName();
				String mimetype = new MimetypesFileTypeMap().getContentType(file);
				String type = mimetype.split("/")[0];
				String extension = fileName.substring(fileName.lastIndexOf('.'));
				if (type.equals("image") || (mimetype.equals("application/octet-stream")
						&& extension.equals(".png"))) {
					try {
						messageLabel.setGraphic(
								new ImageView(new Image(new FileInputStream(message.getFile()),
										IMAGE_WIDTH, IMAGE_HEIGHT, true, true)));
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
				}
				messageLabel.addEventFilter(MouseEvent.MOUSE_CLICKED,
						new EventHandler<MouseEvent>() {
							public void handle(MouseEvent e) {
								ClientApplication.this.downloadFile = file;
								ClientApplication.this.downloadFile();
							}
						});
			}
			this.chatBox.getChildren().add(messageLabel);
		}
	}

	/**
	 * Clear all text from the input area.
	 */
	public void clearInputTextArea() {
		this.inputArea.setText("");
	}

	/**
	 * Get the file that is to be uploaded.
	 * 
	 * @return The file that was selected for upload in the next message.
	 */
	public File getUploadFile() {
		return this.uploadFile;
	}

	/**
	 * Get the file that is to be downloaded.
	 * 
	 * @return The file that was selected for download.
	 */
	public File getDownloadFile() {
		return this.downloadFile;
	}

	/**
	 * Determines if the user has selected a file to include in the next
	 * message.
	 * 
	 * @return true if a file has been selected for sending, false otherwise
	 */
	public boolean hasUploadFile() {
		return this.uploadFile != null;
	}

	/**
	 * Show an error message detailing that the username entered is invalid.
	 */
	public void showUsernameError() {
		this.usernameField.setText("");
		this.showErrorDialog("Invalid username",
				"This username is taken. Please choose a different username.");
	}

	public static void main(String[] args) {
		ClientApplication.launch(args);
	}
}
