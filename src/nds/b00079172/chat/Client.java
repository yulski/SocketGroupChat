package nds.b00079172.chat;

import java.io.*;
import java.net.*;

import javafx.application.Platform;

/**
 * A client connected to a chat server and participating in a chat session.
 * 
 * @author julek
 *
 */
public class Client implements Runnable {

	private Socket socket;
	private String username;
	private ClientApplication app;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private boolean initialized;
	private boolean stopped;

	/**
	 * Create a new Client and connect to the server.
	 * 
	 * @param address
	 *            The IP address of the chat server.
	 * @param port
	 *            The port number that the server is listening on.
	 * @param username
	 *            The username to represent this particular client.
	 * @param app
	 *            The ClientApplication instance this Client is running in.
	 * @throws IOException
	 *             If fails to establish an input/output stream through the
	 *             socket and with the server.
	 */
	public Client(InetAddress address, int port, String username, ClientApplication app)
			throws IOException {
		this.initialized = false;
		this.stopped = false;
		this.socket = new Socket(address, port);
		this.username = username;
		this.output = new ObjectOutputStream(this.socket.getOutputStream());
		this.input = new ObjectInputStream(this.socket.getInputStream());
		this.app = app;
	}

	/**
	 * Disconnect from the server and close input/output streams and the socket
	 * connected to the server.
	 */
	public void disconnect() {
		try {
			this.output.close();
			this.input.close();
			this.socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the username of this client.
	 * 
	 * @return Client's username.
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * Listen for and handle any incoming messages.
	 */
	public void run() {

		if (!this.initialized) {
			throw new RuntimeException("Client uninitialized.");
		}

		new Thread() {
			public void run() {
				while (!Client.this.stopped) {
					try {
						final Message inMsg = (Message) Client.this.input.readObject();
						Platform.runLater(new Runnable() {
							public void run() {
								Client.this.app.addMessageToChatArea(inMsg);
							}
						});
					} catch (IOException e) {
						System.out.println("Client disconnecting...");
						Client.this.stop();
						Client.this.disconnect();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();

	}

	/**
	 * Initialize the client with a username. This method queries the server to
	 * find if the username assigned to this client is available. If the
	 * username is available, the client is successfully initialzed. If the
	 * username is not available, the client is stopped.
	 */
	public void initialize() {
		try {
			Message usernameMessage = new Message(Message.USERNAME_PROPOSAL_MESSAGE, this.username,
					this.username);
			this.output.writeObject(usernameMessage);
			Message usernameResponse = (Message) this.input.readObject();
			if (usernameResponse.getMessageType() == Message.USERNAME_ACCEPTED_MESSAGE) {
				this.initialized = true;
			} else {
				this.stop();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Determines if the client is initialized.
	 * 
	 * @return true if the client has been successfully initialized, false
	 *         otherwise
	 */
	public boolean isInitialized() {
		return this.initialized;
	}

	/**
	 * Determines if the client is stopped.
	 * 
	 * @return true if the client is stopped, false otherwise
	 */
	public boolean isStopped() {
		return this.stopped;
	}

	/**
	 * Send a text message to the chat server.
	 * 
	 * @param text
	 *            The text to put in the message body.
	 */
	public void sendMessage(String text) {
		try {
			Message message = new Message(Message.STANDARD_MESSAGE, this.username, text);
			this.output.writeObject(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send a text message with a different message type to the chat server.
	 * 
	 * @param text
	 *            The text to put in the message body.
	 * @param messageType
	 *            The type of message to send. This must be one of the constants
	 *            defined in the Message class.
	 */
	public void sendMessage(String text, int messageType) {
		try {
			Message message = new Message(messageType, this.username, text);
			this.output.writeObject(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send a message consisting of text and a file to the chat server.
	 * 
	 * @param text
	 *            The text to put in the message body.
	 * @param file
	 *            The file to include in the message.
	 */
	public void sendMessage(String text, File file) {
		try {
			Message message = new Message(Message.STANDARD_MESSAGE, this.username, text, file);
			this.output.writeObject(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send a message with a different type, consisting of text and a file to
	 * the chat server.
	 * 
	 * @param text
	 *            The text to put in the message body.
	 * @param messageType
	 *            The type of message to send. This must be one of the constants
	 *            defined in the Message class.
	 * @param file
	 *            The file to include in the message.
	 */
	public void sendMessage(String text, int messageType, File file) {
		try {
			Message message = new Message(messageType, this.username, text, file);
			this.output.writeObject(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Stop the client listening for incoming messages.
	 */
	public void stop() {
		this.stopped = true;
	}

}
